(function() {
	'use strict';

	let app = angular.module('70kiss',[]);


	app.factory('modelFac', ['$timeout',function(timeout) {
		/**
		 * 
		 * @param
		 * @return
		 */
		class Model1 {
			constructor(config) {
				this.data = config.data;
			}	
			init() {
				// do something
				this.expo();
				timeout(()=> {
					console.log('asdf');
					this.data.wordCount[5].border = '3px dotted red';
				}, 2000); 
			}

			initCircleStyle() {

			}

			expo() {
				let d
				if(this.data) {
					for(let i = 0; i < this.data.wordCount.length; i++) {
						d = this.data.wordCount[i];
						d.count *= d.count;
						d.width = d.count / 100;
						d.height = d.width;
						d.border = '1px solid green';
						d.borderRadius = d.width / 2;
					}
				}
			}
		}

		/**
		 * 
		 * @param
		 * @return
		 */
		class Model2{
			constructor(config) {
				this.data = config.data;
			}
			init() {
				// do something
			}
		}

		return {
			instanctes: {},
			lib: {
				Model1: Model1,
				Model2: Model2
			},
			idGen: () => {
				let id = 'fooo';
				return id;
			},
			create(name, config) {
				return this.lib[name] && (() => {
					return new this.lib[name](config);
				})();
			}
			
		}
	}]);


	app.controller('appController', [
		'$scope', 
		'$http', 
		'modelFac', function(s, http, modelFac) {

		let init = () => {
			let p = fetchData();
			p.then(onSuccessFetchData);
		}

		let onSuccessFetchData = (res) => {
			createModel1(angular.copy(res.data[0]));
			createModel2(angular.copy(res.data[0]));
		}

		let fetchData = () => {
			return http({
				method: 'GET',
				url: 'cloud.json'
			});
		}

		/**
		 * 
		 * @param
		 * @return
		 */
		let createModel1 = (data) => {
			// model 1
			s.model1 = modelFac.create('Model1', {
				data: data
			});
			
			s.model1.init();
			return s.model1;
		}

		/**
		 * 
		 * @param
		 * @return
		 */
		let createModel2 = (data) => {
			// model 2
			s.model2 = modelFac.create('Model2', {
				data: data
			});
			return s.model2;
		}

		init();
	}]);
})();